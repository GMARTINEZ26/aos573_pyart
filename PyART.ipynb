{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f8bed61f-2d53-4879-8af0-00cd99f16302",
   "metadata": {
    "tags": []
   },
   "source": [
    "<b><font size=10, color='#4ba3b4'> Python ARM Radar Toolkit -- Py-ART"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7341dfcd-2339-4538-b8c3-fc4c26b9c7a9",
   "metadata": {},
   "source": [
    "Giselle Martinez</br>\n",
    "Nov. 28, 2022</br>\n",
    "AOS573"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "87fe9274-027c-49ed-8d25-ec5ec9ed749c",
   "metadata": {},
   "source": [
    "## Background"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5cc9a244-6467-4fab-92c7-6e8f8abb0150",
   "metadata": {},
   "source": [
    "Today we will learn about the Python ARM Radar Toolkit, or Py-ART for short. This module was designed to contain weather radar algorithms and other utilities, \n",
    "(one of which is their RadarDisplay function that we'll use later). This module is used by the [Atmospheric Radiation Measurement (ARM) Climate Research Facility](https://www.arm.gov/), and where satellite data is currently retrieved from. \n",
    "\n",
    "More information about ARM is located below, most of today's lecture is also retrieved from the AMS Short Course on Source Radar Software."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03d154a4-ddae-4940-9e6f-cab5f9dfe1be",
   "metadata": {},
   "source": [
    "#### References\n",
    "[AMS Short Course on Open Source Radar Software](https://github.com/openradar/AMS-Short-Course-on-Open-Source-Radar-Software)</br>\n",
    "[AMS/ASR Meeting Py-ART, the Python ARM Radar Toolkit](https://github.com/ANL-DIGR/notebooks/tree/master/ASR_PI_2015)</br>\n",
    "[AMS/ASM Meeting, Py-ART Tutorial](https://github.com/ANL-DIGR/notebooks/tree/master/ASR_PI_2014)</br>\n",
    "[NWS Area Radar](https://www.weather.gov/cle/Area_Radars)\n",
    "\n",
    "#### Video References\n",
    "[Python ARM Radar Toolkit Tutorial](https://www.youtube.com/watch?v=diiP-Q3bKZw&t=274s)</br>\n",
    "[Plotting NEXRAD Data with Python](https://www.youtube.com/watch?v=f_y1pmodyIc&t=1s) <u> (Very helpful for this notebook as we'll be plotting NEXRAD Data with Py-ART.) </u>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "650f25d8-bbe4-4798-b7ff-848a00f07b02",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import cartopy.crs as ccrs\n",
    "\n",
    "import pyart\n",
    "from pyart.testing import get_test_data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ae17464-a367-41ab-af4d-da539d060fbb",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Reading in NEXRAD data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc2abb62-1cf9-41c3-b695-92714fa72edf",
   "metadata": {},
   "source": [
    "Py-ART can read in files in several formats, for the sake of time, we'll be using a NEXRAD data given by the Next Generation Weather Radar (NEXRAD) Data from Amazon Web Services, [NEXRAD Data](https://www.ncei.noaa.gov/access/metadata/landing-page/bin/iso?id=gov.noaa.ncdc:C00345) and if you are interested on gathering any data file, you can visit their [NEXRAD Data Archive](https://www.ncdc.noaa.gov/nexradinv/):\n",
    "\n",
    "The following template of the data retrieved from [py-ART, reading NEXRAD data](https://arm-doe.github.io/pyart/examples/io/plot_nexrad_data_aws.html):\n",
    "```bash\n",
    "s3://noaa-nexrad-level2/year/month/date/station/{station}{year}{month}{date}_{hour}{minute}{second}_V06\n",
    "```\n",
    "\n",
    "Today we'll look at radar data from <b> Green Bay, WI, (KGRB) on November 15, 2022 at 1207 UTC.</b>\n",
    "\n",
    "And use `pyart.io.read_nexrad_archive` to access the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2f50dd06-8f64-4568-b282-9a99c573aeaa",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_KGRB = pyart.io.read_nexrad_archive('s3://noaa-nexrad-level2/2022/11/15/KGRB/KGRB20221115_120738_V06')\n",
    "data_KGRB"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4773e3ce-0ffb-48b4-84da-185d77f9ea2f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# List of scans and fields needed to use Py-ART radar display \n",
    "list(data_KGRB.fields)\n",
    "\n",
    "#or \n",
    "#data_KGRB.info('standard')\n",
    "\n",
    "#Can see fields units, max and min values, etc.\n",
    "#Also lists other variables the radar data has, (e.g altitude, azimuth, number of sweeps.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76452a96-eecf-4878-94bd-3d8a1da0e27e",
   "metadata": {},
   "source": [
    "Using these fields, data can be plotted using Py-ART's [RadarMapDisplay](https://arm-doe.github.io/pyart/API/generated/pyart.graph.RadarMapDisplay.html) in the form of a radar.\n",
    "\n",
    "* spectrum_width\n",
    "* differential_phase\n",
    "* reflectivity\n",
    "* cross_correlation_ratio\n",
    "* clutter_filter_power_removed\n",
    "* differential_reflectivity\n",
    "* velocity\n",
    "\n",
    "To display NEXRAD radar data, we'll use `pyart.graph.RadarMapDisplay`, and set it as a variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91e59110-1ba0-4ff0-8fe6-336e78d4f486",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_KGRB = pyart.graph.RadarMapDisplay(data_KGRB)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ff562b8-44b5-4eef-b9ed-93bfd13e7884",
   "metadata": {},
   "source": [
    "## Plotting NEXRAD Data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22a4f119-1ef7-4ded-826d-b641b89e220f",
   "metadata": {},
   "source": [
    "Here we'll use [plot_ppi_map](https://arm-doe.github.io/pyart/API/generated/pyart.graph.RadarMapDisplay.plot_ppi_map.html):\n",
    "\n",
    "<b>We have to include the field</b>, (from above) also the <b>sweep number</b> to plot. That's where `data.info` comes in handy. \n",
    "We can locate how many number of sweeps the data has."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9905d4d-92a0-469d-bb3e-3d7a3607c9dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12, 4))\n",
    "ax = plt.subplot(121, projection=ccrs.PlateCarree())\n",
    "\n",
    "display_KGRB.plot_ppi_map('reflectivity', sweep=0, ax=ax, vmin=-30, vmax=90, #field, sweep,\n",
    "                     min_lat=41, max_lat=46.5, min_lon=-83, max_lon=-93,\n",
    "                     colorbar_label='Reflectivity [dBZ]')\n",
    "\n",
    "ax = plt.subplot(122, projection=ccrs.PlateCarree())\n",
    "display_KGRB.plot_ppi_map('velocity', sweep=1, ax=ax, vmin=-30, vmax=30, #field, sweep,\n",
    "                     min_lat=41, max_lat=46.5, min_lon=-83, max_lon=-93,\n",
    "                     colorbar_label='Velocity [m/s]', cmap=plt.cm.PuOr)\n",
    "\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "340c0c90-1ba0-499f-b6b0-05d9037bcc04",
   "metadata": {},
   "source": [
    "* Reflectivity plot doesn't show much to work with.\n",
    "* But the velocity plot shows a pattern that resembles rotation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91ccca38-00bd-4331-9a3d-e65e75edf27f",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb332b14-102f-45aa-bb76-7242ec6ab7b0",
   "metadata": {},
   "source": [
    "## Brief Background on Radar Scans"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad272266-2a40-4566-9392-5220885ade2b",
   "metadata": {},
   "source": [
    "For those who are new to radar, reflectivity can show us specific types of precipitation (e.g, hail, snow, rain). Velocity as shown here is the target motion, indicated as <i> away </i> and <i> toward </i> the radar. More information can be found in [NWS Area Radar](https://www.weather.gov/cle/Area_Radars)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91a01c6a-f71e-479b-9bb8-cbaf11043087",
   "metadata": {},
   "source": [
    "Two scanning modes that are common in meteorology are: [Plan Position Indicator (PPI)](https://en.wikipedia.org/wiki/Plan_position_indicator) and the [Range Height Indicator (RHI)](https://glossary.ametsoc.org/wiki/Range-height_indicator) scans. Py-ART can plot cross sections from both RHI and PPI, however, since we are retriving NEXRAD data, we can only apply a cross section from a PPI volume."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68b5b2f3-a6da-4f0e-ae62-de56cfd543e1",
   "metadata": {},
   "source": [
    "## Plotting Cross Sections"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4f23554-cc56-46c0-85bf-7661a4f3d09c",
   "metadata": {},
   "source": [
    "Extracts a cross section from two azimuth angles and a volume of PPI scans, from [PPI Cross Sections](https://arm-doe.github.io/pyart/examples/plotting/plot_xsect.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c94c3a2a-27f7-4db6-93e5-4a84439f10f8",
   "metadata": {},
   "outputs": [],
   "source": [
    "cross_section_KGRB = pyart.util.cross_section_ppi(data_KGRB, [0, 360]) #data, azimuth angles"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f55a527c-6aca-401f-b2da-2bb1fa408fa1",
   "metadata": {},
   "source": [
    "Use [RadarDisplay](https://arm-doe.github.io/pyart/API/generated/pyart.graph.RadarDisplay.html):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b74bd81e-588e-45e6-9f5e-1c8aef176b9b",
   "metadata": {},
   "outputs": [],
   "source": [
    "cross_display_KGRB = pyart.graph.RadarDisplay(cross_section_KGRB)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17b57a3d-36da-4558-b291-a6b22e22d91d",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12,2))\n",
    "cross_display_KGRB.plot('reflectivity', sweep=0, vmin=-30, vmax=90, \n",
    "             colorbar_label='Reflectivity [dBZ]')\n",
    "plt.xlim(0,130)\n",
    "plt.ylim(0,9)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cf292546-accd-4710-a77d-f50eb219fd3e",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12,2))\n",
    "cross_display_KGRB.plot('velocity', sweep=1, vmin=-90, vmax=90, \n",
    "             colorbar_label='Velocity [m/s]', cmap=plt.cm.PuOr)\n",
    "plt.xlim(0,130)\n",
    "plt.ylim(0,9)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "151c41fd-8177-477f-8903-d3f03217719e",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50f42f59-6c4e-472d-abc4-945c2abf4b16",
   "metadata": {},
   "source": [
    "Both cross sections don't give you much information to work with, I ran another data file to plot another cross section to see if we could gather more information from the plot.\n",
    "\n",
    "Here we'll use, <b> Des Moines, IA, (KDMX) on November, 04, 2022 at 2357 UTC (5:57 CST) </b> during a storm outbreak."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad094d9c-fbb8-41b7-b31a-b6cb1ec82d38",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_KDMX = pyart.io.read_nexrad_archive('s3://noaa-nexrad-level2/2022/11/04/KDMX/KDMX20221104_235700_V06')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f087bf2f-5ff3-4922-bfbc-976219e84e07",
   "metadata": {},
   "outputs": [],
   "source": [
    "list(data_KDMX.fields)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bcdcce66-413d-4d83-a1f5-a56e65db039b",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_KDMX = pyart.graph.RadarMapDisplay(data_KDMX)\n",
    "\n",
    "fig = plt.figure(figsize=(12, 4))\n",
    "ax = plt.subplot(121, projection=ccrs.PlateCarree())\n",
    "\n",
    "display_KDMX.plot_ppi_map('reflectivity', sweep=0, ax=ax, vmin=-30, vmax=90,\n",
    "                          min_lat=39, max_lat=44, min_lon=-89.5, max_lon=-97,\n",
    "                          colorbar_label='Reflectivity [dBZ]')\n",
    "\n",
    "ax = plt.subplot(122, projection=ccrs.PlateCarree())\n",
    "display_KDMX.plot_ppi_map('velocity', sweep=1, ax=ax, vmin=-50, vmax=50,\n",
    "                          min_lat=39, max_lat=44, min_lon=-89.5, max_lon=-97,\n",
    "                          colorbar_label='Velocity [m/s]', cmap=plt.cm.RdBu)\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad60fe8c-fd26-4578-a08f-838c4d6b32fb",
   "metadata": {},
   "source": [
    "* Large section of reflectivity around 40 dBZ to 60 dBZ.\n",
    "* Velocity has a lower resolution, though a particular feature at 41.5 N, 93.5 W could be of note as a possible location for inflow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eb7ce9f0-2d47-4236-b444-245b564469e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "cross_section_KDMX = pyart.util.cross_section_ppi(data_KDMX, [0, 360]) #azimuth angles"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9563db97-7b2d-4032-9232-64fbdd4d9ecb",
   "metadata": {},
   "outputs": [],
   "source": [
    "cross_display_KDMX = pyart.graph.RadarDisplay(cross_section_KDMX)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a677e8cc-ea0e-4d7c-8fda-7edeb0873cbf",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12,2))\n",
    "cross_display_KDMX.plot('reflectivity', sweep=1, vmin=-30, vmax=50, \n",
    "             colorbar_label='Reflectivity [dBZ]')\n",
    "plt.xlim(0,90)\n",
    "plt.ylim(0,10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f609fc2-8c82-492d-8b3e-6e06c0d43ed3",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12,2))\n",
    "cross_display_KDMX.plot('velocity', sweep=1, vmin=-50, vmax=50, \n",
    "             colorbar_label='Velocity [m/s]', cmap=plt.cm.RdYlBu)\n",
    "plt.xlim(0,90)\n",
    "plt.ylim(0,10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09af577e-4ae3-49db-8b42-540f55bedfb2",
   "metadata": {},
   "source": [
    "The cross sections for Des Moines, IA looks a bit better, unfortunely, the resolution is lower using PPI. If you are interested in high-res plotting with RHI, you may use [this tutorial](https://arm-doe.github.io/pyart/examples/plotting/plot_rhi_two_panel.html), however you would have to use CF/Radial file format for your data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "71b69760-d616-447a-bd42-c4a834e0c0f7",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "95e1a69b-42df-4ab1-9d70-496e646aab18",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Plotting - Des Moines, IA"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8fef4a0e-843b-465d-9844-c16f56885ce3",
   "metadata": {},
   "source": [
    "Cross sections can tell us about the evolution of a storm, I marked a few points that I found interesting and plotted <b> Des Moines, IA, (KDMX) on the same day, approx. two hours later at 0153 UTC (7:53 CST). </b> "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "314ad3c5-c27e-47db-b61f-fa90975126cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_KDMX0153 = pyart.io.read_nexrad_archive('s3://noaa-nexrad-level2/2022/11/05/KDMX/KDMX20221105_015322_V06')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58554111-8bd0-40e1-a3c8-cd69b4894e10",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_KDMX0153 = pyart.graph.RadarMapDisplay(data_KDMX0153)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ae688e55-0529-42a7-9c7f-a09d2067c093",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12, 4))\n",
    "ax = plt.subplot(121, projection=ccrs.PlateCarree())\n",
    "\n",
    "display_KDMX0153.plot_ppi_map('reflectivity', sweep=1, ax=ax, vmin=-30, vmax=90,\n",
    "                    min_lat=39, max_lat=44, min_lon=-89.5, max_lon=-97,\n",
    "                     colorbar_label='Reflectivity [dBZ]')\n",
    "\n",
    "ax = plt.subplot(122, projection=ccrs.PlateCarree())\n",
    "display_KDMX0153.plot_ppi_map('velocity', sweep=1, ax=ax, vmin=-50, vmax=50,\n",
    "                    min_lat=39, max_lat=44, min_lon=-89.5, max_lon=-97,\n",
    "                     colorbar_label='Velocity [m/s]', cmap=plt.cm.RdBu)\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a77b4826-5485-418d-86dc-0c30497feb35",
   "metadata": {},
   "source": [
    "Plot using `pyart.graph.RadarDisplay` again:\n",
    "\n",
    "Shows the field at a distance from the radar."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "68036a1c-fa62-42e2-a799-270eb14cb054",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_display_KDMX0153 = pyart.graph.RadarDisplay(data_KDMX0153)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "428ed68c-26d6-4463-905a-c287eaa4ff06",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(15, 10))\n",
    "ax = plt.subplot(221)\n",
    "plot_display_KDMX0153.plot('reflectivity', sweep=1, ax=ax, vmin=-20, vmax=90, \n",
    "             colorbar_label='Reflectivity [dBZ]')\n",
    "plt.xlim(-100,250)\n",
    "plt.ylim(-250,250)\n",
    "\n",
    "\n",
    "ax = plt.subplot(222)\n",
    "plot_display_KDMX0153.plot('velocity', sweep=1, ax=ax, vmin=-30, vmax=90, \n",
    "            colorbar_label='Velocity [m/s]', cmap=plt.cm.RdBu)\n",
    "plt.xlim(-100,250)\n",
    "plt.ylim(-250,250)\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "768ef14e-f59f-4c77-b54e-c8ee3fbb37b5",
   "metadata": {},
   "source": [
    "* Storm moving east.\n",
    "* Same feature at 41.5 N, 93.5 W possible rotation?\n",
    "* 42 N, 95 W, another feature in the velocity plot that may be rotation.\n",
    "\n",
    "Py-ART is useful to find any feature in NEXRAD data, the downside is the low resolution. Using other formats for radar data could solve this issue, but it is still valuable for research that involves radar or satellite data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "223bff2e-8717-48c9-b0ba-95e3af8a1f7b",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "cross_section_KDMX0153 = pyart.util.cross_section_ppi(data_KDMX0153, [0, 360]) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "55715cd9-35e8-4d7b-8b50-b86c5b1996f2",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "cross_display_KDMX0153 = pyart.graph.RadarDisplay(cross_section_KDMX0153)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21eead9c-0fdb-4dd0-bf33-e4e0e1412e1a",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(15, 10))\n",
    "ax = plt.subplot(411)\n",
    "cross_display_KDMX0153.plot('reflectivity', sweep=1, ax=ax, vmin=-20, vmax=90, \n",
    "             colorbar_label='Reflectivity [dBZ]')\n",
    "plt.xlim(0,200)\n",
    "plt.ylim(0,10)\n",
    "\n",
    "ax = plt.subplot(412)\n",
    "cross_display_KDMX0153.plot('velocity', sweep=1, ax=ax, vmin=-30, vmax=90, \n",
    "            colorbar_label='Velocity [m/s]', cmap=plt.cm.RdBu)\n",
    "plt.xlim(0,200)\n",
    "plt.ylim(0,10)\n",
    "\n",
    "ax = plt.subplot(413)\n",
    "cross_display_KDMX0153.plot('differential_phase', sweep=1, ax=ax, vmin=-30, vmax=90, \n",
    "            colorbar_label='Differential Phase [deg]', cmap=plt.cm.YlGn)\n",
    "plt.xlim(0,200)\n",
    "plt.ylim(0,10)\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e95a66eb-2bf4-4dd8-a822-3b0b763f5e50",
   "metadata": {},
   "source": [
    "In cross sections, especially for real-life observations, it is common to have missing data. For my research, dealing with missing data (e.g wind profiles) are attained by dropsonde data and can help to fill in missing information."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76ca1389-080e-4197-8fc6-73c0b2f78f77",
   "metadata": {},
   "source": [
    "---"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "AOS573-Tutorials",
   "language": "python",
   "name": "aos573-tutorials"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
